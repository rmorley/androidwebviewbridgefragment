# An Android WebView Javascript Bridge #

This repository contains an Andriod WebView bridge for communicating between Android and WebView-hosted Javascript. Some of the features of this bridge include:

* Loads HTML and can control how it caches content for offline use.
* Facilitates calls from native to WebView-hosted JavaScript including return values.
* Facilitates calls from WebView-hosted JavaScript to native including return values.
* JavaScript library is compatible with the [iOS Bridge](https://bitbucket.org/rmorley/ioswebviewbridge/overview).
* A simple utility to read bitmaps from files then resize them.

### Setup ###
1. Add this project to your Workspace
2. In your Android project's Properties, select Android and under 'Library' add 'AndroidWebviewBridgeFragment'.
3. Implement NativeControllable on the Activity that will instantiate the bridge fragment.
3. Instantiate either 
    1. WebViewBridgeFragment for bundled (local file system) HTML files using the non-default constructor, or
    2. ReloadableWebViewBridgeFragment for network-accessed HTML files using the non-default constructor making reference to an Activity resource layout with optionally one or more of the following widgets to facilitate loading and reloading: 
        1. a Button tagged "buttonLoad", 
        2. a ProgressBar tagged "progressLoading", and 
        3. a TextView tagged "textLoadError".
6. Reference js/bridge.js in your JavaScript 

### Use ###
* From Android Activity, make calls into JavaScript using 
```
#!java

webViewControllable.callWebViewJs("JSFUNCTION(PARAMS)", WebViewJsCallReturnHandler);
```

* From Javascript, call into Android by invoking:
```
#!JavaScript

Bridge.exec(successHandlerCallback, errorHandlerCallback, "SERVICENAME", "ACTIONNAME", ["PARAM1", "PARAM2", ... ]);
```

* Calls into Android result in calls to Activity's implementation of:

```
#!java

NativeControllable.callNative(int webViewBridgeFragmentId, String service, String action, List<String> params, NativeCallReturnHandler nativeCallReturnHandler);
```



### References ###
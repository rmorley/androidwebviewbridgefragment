/*
   Copyright 2014 Compass Point, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

var Bridge = {
	requests: {},
	nextRequestId: 0,
	exec: function(success, failure, service, action, params, timeoutMillis) {
		if (typeof timeoutMillis === 'undefined' ) timeoutMillis = 10000;
		
		var requestId = ++this.nextRequestId;
		var request = {
			success: success,
			failure: failure,
			pending: true,
			requestJson: JSON.stringify({
				requestId: requestId,
				service: service,
				action: action,
				params: params,
			}) 
		};
		this.requests[requestId] = request;
		var requests = this.requests;
		var exec = function() {
			if (isIOS8OrGreater()) {
				try {
	        		window.webkit.messageHandlers.callNative.postMessage(request.requestJson); //Allowed types for are NSNumber, NSString, NSDate, NSArray, NSDictionary, and NSNull.
	        		request.pending = false;
                } catch(err) {
	        		if (failure) {
	        			failure(err);
	        		}
        			delete requests[requestId]
	        		return false;
	    		}
			} else if(isAndroid()) {
				try {
					window.JavaHost.callNative(request.requestJson);
	        		request.pending = false;
	    		} catch(err) {
	        		if (failure) {
	        			failure(err);
	        		}
        			delete requests[requestId]
	        		return false;
	    		}
			} else {  //iOS < v8 or some other browser??
				var iframe = document.createElement("IFRAME");
		    		iframe.setAttribute("src", "processrequestsfromjs:");
		    		document.documentElement.appendChild(iframe);
		    		iframe.parentNode.removeChild(iframe);
		    		iframe = null;
		    		//return false;
			}
			return true;
		}
		if (!exec()) {
			return;
		} else {
			//wait for exec to return
			var totalIntervals = timeoutMillis / 200;
			var intervalId = setInterval(function() {
				var request = requests[requestId];
				if (!request) {
					console.log("no request");
					clearInterval(intervalId);
				} else if ('returnValue' in request) {
					clearInterval(intervalId);
					delete requests[requestId];
					var sec = (timeoutMillis/200 - totalIntervals)*200;
					console.log("Return value found for requestId: " + requestId + "; returnValue=" + request.returnValue + "' isError=" + request.isError + ". Total elapsed time in seconds: " + sec);
					if (request.isError) {
						if (failure) {
							failure(request.returnValue);
						}
					} else {
						if (success) {
							success(request.returnValue);
						}
					}
				} else if (totalIntervals-- == 0) {
					clearInterval(intervalId);
					delete requests[requestId];
					if (failure) {
						failure("waiting for result timeout");
					}
//				} else if (totalIntervals == 10) {
//					Bridge.returnValueForJsRequest(requestId, "boo");
				} else {
//					console.log("Waiting for return from request " + requestId + ". Iterations remaining: " + totalIntervals);
				}
			} , 50);
		}
	},
	returnValueForJsRequest: function(requestId, returnValue, isError) {
		console.log("Received return for requestId " + requestId + ". returnValue: " + returnValue);
		var request = this.requests[requestId];
		if (request) {
			request.returnValue = returnValue;
			request.isError = isError;
		}
	},
	getPendingJsRequest: function() {
		for (var requestId in this.requests) {
		  if (requestId && this.requests[requestId].pending) {
			  var request = this.requests[requestId];
			  this.requests[requestId].pending = false;
			  //delete this.requests[requestId];
			  return request.requestJson;
		  }
		}		
		return '';
	}
};

function iOSversion() {
    if (/iP(hone|od|ad)/.test(navigator.platform)) {
        // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
        var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
        return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
    } else {
        return null;
    }
}

var isIOS8OrGreater = function() {
//  var deviceAgent = navigator.userAgent.toLowerCase();
//  return /iphone os 8_/.test(deviceAgent);
    var ver = iOSversion();
    if (ver && (ver[0] >= 8)) {
        return true;
    } else {
        return false;
    }
}

var isAndroid = function() {
   return (navigator.userAgent.indexOf("Android") >= 0);
}


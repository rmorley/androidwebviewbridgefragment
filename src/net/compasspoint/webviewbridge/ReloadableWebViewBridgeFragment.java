/*
   Copyright 2014 Compass Point, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package net.compasspoint.webviewbridge;


import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ReloadableWebViewBridgeFragment extends WebViewBridgeFragment {

	protected static final String FRAGMENT_LAYOUT_ID = "fragment_layout_id";
	public static final String BUTTON_LOAD_TAG = "buttonLoad";
	public static final String PROGRESS_LOADING_TAG = "progressLoading";
	public static final String TEXT_LOAD_ERROR_TAG = "textLoadError";

	/**
	 * This fragment's factory method. Activities attaching to this fragment must 
	 * implement the NativeControllable interface.
	 * @param fragmentId Must uniquely identify fragment for activity. Supplied on callbacks to identify this fragment as caller. 
	 * @param fragmentLayoutId Identifies id of the layout that webView will be added to. Layout can optionally contain one 
	 * or more of the following widgets to facilitate loading
	 * and reloading: (1) a Button tagged "buttonLoad", (2) a ProgressBar tagged "progressLoading", and (3) a TextView tagged "textLoadError".
	 */
	public ReloadableWebViewBridgeFragment(int fragmentId, int fragmentLayoutId) {
		super(fragmentId);
		Bundle args = getArguments();
		args.putInt(FRAGMENT_LAYOUT_ID, fragmentLayoutId);
		setArguments(args);
	}
	
	public ReloadableWebViewBridgeFragment() {
		
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		int fragmentLayoutId = getArguments().getInt(FRAGMENT_LAYOUT_ID);
		XmlResourceParser layoutParser = getActivity().getResources().getLayout(fragmentLayoutId);
        final View view = inflater.inflate(layoutParser, container, false);
		final Button buttonLoad = (Button)view.findViewWithTag(BUTTON_LOAD_TAG);  
		final ProgressBar progressLoading = (ProgressBar)view.findViewWithTag(PROGRESS_LOADING_TAG);
		final TextView textLoadError = (TextView)view.findViewWithTag(TEXT_LOAD_ERROR_TAG);  

        webView.setWebViewClient(new WebViewClient() {
			private int ignoreFinishedCount = 0;
			private boolean isFirst = true;

			{ // an anonymous "instance initializer"
				 if (buttonLoad != null) {
					 buttonLoad.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) { 
							   if (progressLoading != null) {
								   progressLoading.setVisibility(View.VISIBLE);
							   }
							   if (textLoadError != null) {
								   textLoadError.setVisibility(View.INVISIBLE);
							   }
							   buttonLoad.setVisibility(View.INVISIBLE);
							   
							   loadWebview(webView, getNativeController().getLoadableUrl(getArguments().getInt(FRAGMENT_ID)), true);
	
						}
					 });
				 }
			}
			
			@Override
			public void onPageStarted(WebView webView, String url, Bitmap favicon) {
				Log.d(this.getClass().getName(), "onPageStarted: " + url);
				//super.onPageStarted(view, url, favicon);
			
				if (this.ignoreFinishedCount == 0) {
					webView.setVisibility(View.INVISIBLE);
				}
				if (isNativeControllableWebViewControllableSet)
				{
					getNativeController().unsetWebViewControllable(getArguments().getInt(FRAGMENT_ID));
					isNativeControllableWebViewControllableSet = false;
				}
			}
		   
		   @Override
			public void onPageFinished(WebView view, String url) {
				//view.setVisibility(View.VISIBLE);
				Log.d(this.getClass().getName(), "onPageEnded: " + url);

				if (this.ignoreFinishedCount == 0) {
					Log.d(this.getClass().getName(), "onPageEnded ignoreFinishedCount == 0. setting webview to visible");
					view.setVisibility(View.VISIBLE);
					if (!isNativeControllableWebViewControllableSet)
					{
						if (isResumed()) {
							getNativeController().setWebViewControllable(getArguments().getInt(FRAGMENT_ID), ReloadableWebViewBridgeFragment.this);
						}
						isNativeControllableWebViewControllableSet = true;
					}
					fadeInWebViewOnce();
				} else {
					this.ignoreFinishedCount--; 
				}
				//super.onPageFinished(view, url);
			}
		   
			protected void fadeInWebViewOnce() {
				if (isFirst) {
					isFirst = false;
					RelativeLayout layout = (RelativeLayout) view;
					Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
					fadeInAnimation.setAnimationListener(new AnimationListener() {         
					    public void onAnimationStart(final Animation arg0) {}           
					    public void onAnimationRepeat(final Animation arg0) {}          
					    public void onAnimationEnd(final Animation arg0){}
					});
					layout.startAnimation(fadeInAnimation);
				}
			}
			
		   public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			   if (progressLoading != null) {
				   progressLoading.setVisibility(View.INVISIBLE);
			   }
			   if (textLoadError != null) {
				   textLoadError.setVisibility(View.VISIBLE);
				   textLoadError.setText(description);
			   }
			   if (buttonLoad != null) {
				   buttonLoad.setVisibility(View.VISIBLE);
			   }
			   
			   this.ignoreFinishedCount = 2; 
			   Log.d(this.getClass().getName(), "onReceivedError: description: " + description + "; failingUrl: " + failingUrl);
		   }
		   
		   @Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
			   Log.d(this.getClass().getName(), "shouldOverrideUrlLoading: " + url);
			    //return true means the host application handles the url, while return false means the current WebView handles the url.
			   for (String prefix : urlPrefixesAllowedToOpenInBrowser) {
				   if (url.startsWith(prefix)) {
					   Uri uri = Uri.parse(url);
					   Intent intent = new Intent(Intent.ACTION_VIEW, uri);
					   startActivity(intent);
					   break;
				   }
			   }
			   
			   
			   return true;
			}
		 });

        
        webView.setVisibility(View.INVISIBLE);

        RelativeLayout layout = (RelativeLayout) view;
		webView.setLayoutParams(new LayoutParams(
			 LayoutParams.MATCH_PARENT,
			 LayoutParams.MATCH_PARENT));
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		layout.addView(webView);
		
		loadWebview(webView, getNativeController().getLoadableUrl(this.getArguments().getInt(FRAGMENT_ID)), false);

        return view;
    }
}

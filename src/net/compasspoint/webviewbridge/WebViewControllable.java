/*
   Copyright 2014 Compass Point, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package net.compasspoint.webviewbridge;

/**
 * Interface for native to call a fragment's JavaScript.
 * @author R
 *
 */
public interface WebViewControllable
{
	/**
	 * Used to call a fragment's JavaScript
	 * @param functionStatement a JavaScript function that evaluates to a string return value.
	 * @param webViewJsCallReturnHandler called back with JavaScript's return value.
	 * @throws Exception
	 */
	public void callWebViewJs(String functionStatement, WebViewJsCallReturnHandler webViewJsCallReturnHandler) throws Exception;
}

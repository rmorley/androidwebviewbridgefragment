/*
   Copyright 2014 Compass Point, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package net.compasspoint.webviewbridge;

/**
 * Provided when NativeControllable.nativeCall invoked and used to return value to calling JavaScript
 * @author R
 *
 */
public interface NativeCallReturnHandler 
{
	/**
	 * returns value to calling JavaScript.
	 * @param returnValue
	 * @param isError
	 * @throws Exception
	 */
	public void handleReturnFromNative(String returnValue, boolean isError) throws Exception;
}

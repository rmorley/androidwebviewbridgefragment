/*
   Copyright 2014 Compass Point, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package net.compasspoint.webviewbridge;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import net.compasspoint.webviewbridge.R;


import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.webkit.GeolocationPermissions;
import org.json.*;

public class WebViewBridgeFragment extends Fragment implements WebViewControllable {

	
	private static final String kReturnValueForJsRequest = "Bridge.returnValueForJsRequest(%s, '%s', %s)";
	
	/**
	 * A list of URLs that are allowed to open in a separate browser when navigated to in WebView's loaded content.
	 */
	protected List<String> urlPrefixesAllowedToOpenInBrowser = new ArrayList<String>();
	private WebViewJsMessagesHandler webViewJsMessagesHandler = null;
	protected CallJSInterface callJsInterface = null;
	
	protected WebView webView = null;
	protected boolean isNativeControllableWebViewControllableSet = false;
	
	public enum LoadMode {
		LoadNormal,
		LoadCacheFirstWhenNoInet
	}
	
	protected static final String FRAGMENT_ID = "fragment_id";

	/**
	 * This fragment's factory method. 
	 * Activities attaching to this fragment must implement the NativeControllable interface.
	 * @param fragmentId Must uniquely identify fragment for activity. Supplied on callbacks to identify this fragment as caller. 
	 */
	public WebViewBridgeFragment(int fragmentId) {
		Bundle args = new Bundle();
		args.putInt(FRAGMENT_ID, fragmentId);
		setArguments(args);
	}
	
	public WebViewBridgeFragment() {
		
	}
	
    private NativeControllable nativeController;

    public NativeControllable getNativeController() {
    	return nativeController;
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
        	nativeController = (NativeControllable) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement WebViewHandler");
        }
    }

    @Override
    public void callWebViewJs(String functionStatement, WebViewJsCallReturnHandler webViewJsCallReturnHandler) throws Exception
    {
    	this.callJsInterface.callJs(functionStatement, webViewJsCallReturnHandler);
    }

    public static class LoadableUrl {
		private String url;
		
		private LoadMode loadMode;

		public String getUrl() {
			return url;
		}

		public LoadMode getLoadMode() {
			return loadMode;
		}

		public LoadableUrl(String url, LoadMode loadMode) {
			super();
			this.url = url;
			this.loadMode = loadMode;
		}
	}
	
	protected static class CallJSInterface {
		 
		/** The javascript interface name for adding to web view. */
		private final String interfaceName = "JavaHost";
		private WebView webView = null;
		private WebViewJsMessagesHandler webViewJsMessagesHandler = null;
		private Map<Integer, WebViewJsCallReturnHandler> requests = new Hashtable<Integer, WebViewJsCallReturnHandler>();
		private int nextRequestId = 0;

		
		public void loadUrl(String url) {
			this.webView.loadUrl(url);
		}

		public CallJSInterface(WebViewJsMessagesHandler webViewJsMessagesHandler, WebView webView) {
			this.webViewJsMessagesHandler = webViewJsMessagesHandler;
			this.webView = webView;
		}
		
		/**
		 * 
		 * @param functionStatement a JavaScript function that evaluates to a string return value.
		 * @param webViewJsCallReturnHandler called back with JavaScript's return value.
		 * @throws Exception
		 * @throws JavascriptException
		 * @throws JavascriptCallTimeoutException
		 */
		public void callJs(String functionStatement, WebViewJsCallReturnHandler webViewJsCallReturnHandler) throws Exception
	    {
	    	Integer requestId = Integer.valueOf(nextRequestId++);
	    	
			boolean isCalledFromMainUiThread = (Looper.myLooper() == Looper.getMainLooper());
			
			if (webViewJsCallReturnHandler != null)
			{
				Log.d(this.getClass().getName(), "Calling JS " + functionStatement + " and handler is not null. Setting handler to requestid " + requestId);
				requests.put(requestId, webViewJsCallReturnHandler);
			}
			else
			{
				Log.d(this.getClass().getName(), "Calling JS " + functionStatement + " and handler null.");
			}
			
			final String url = "javascript:try{window." + interfaceName + ".onJsCallReturn(" + requestId.intValue() + ", false, " + functionStatement + ");"
					+ "} catch(err){window." + interfaceName + ".onJsCallReturn(" + requestId.intValue() + ", true, err.message);}";	
			if (!isCalledFromMainUiThread) {
				webView.post(new Runnable() {
					@Override
					public void run() {
						webView.loadUrl(url);
					}
				});
			} else {
				webView.loadUrl(url);
			}
	    }

		@JavascriptInterface
		public void onJsCallReturn(int requestIdInt, boolean returnValueIsJsException, String returnValue)
		{
			Integer requestId = Integer.valueOf(requestIdInt);
			WebViewJsCallReturnHandler webViewJsCallReturnHandler = this.requests.get(requestId);
			Log.d(this.getClass().getName(), "BOO!" + returnValue + "; " + requestId);
			if (webViewJsCallReturnHandler != null)
			{
				webViewJsMessagesHandler.obtainMessage(WebViewJsMessagesHandler.jsCallReturnMessage,  0, 0, new WebViewJsMessagesHandler.JsCallReturnMessage(webViewJsCallReturnHandler, returnValue, returnValueIsJsException)).sendToTarget();
				this.requests.remove(requestId);
				Log.d(this.getClass().getName(), "JS returned and posting value to handler. ReturnValue is " + returnValue);
			}
			else if (returnValueIsJsException)
			{
				Log.d(this.getClass().getName(), "JS returned an error but caller did not provide a handler. Error reports: " + returnValue);
			}
			else
			{
				Log.d(this.getClass().getName(), "JS returned but handler is null. ReturnValue is " + returnValue);
			}
		}

		@JavascriptInterface
		public void callNative(String requestJson) throws InterruptedException
		{
			webViewJsMessagesHandler.obtainMessage(WebViewJsMessagesHandler.callNativeMessage,  0, 0, requestJson).sendToTarget();
		}

		public String getInterfaceName()
		{
			return this.interfaceName;
		}
	}

	private static class WebViewJsMessagesHandler extends Handler {
		   public static final int callNativeMessage = 6657;
		  
		   public static final int jsCallReturnMessage = 8832;
	
		   public static class JsCallReturnMessage
		   {
			   private WebViewJsCallReturnHandler webViewJsCallReturnHandler;
			   private String returnValue;
			   private boolean returnValueIsJsException;

			   public JsCallReturnMessage(
					WebViewJsCallReturnHandler webViewJsCallReturnHandler,
					String returnValue, boolean returnValueIsJsException) 
			   {
				   super();
				   this.webViewJsCallReturnHandler = webViewJsCallReturnHandler;
				   this.returnValue = returnValue;
				   this.returnValueIsJsException = returnValueIsJsException;
			   }
			
			   public WebViewJsCallReturnHandler getWebViewJsCallReturnHandler() 
			   {
				   return webViewJsCallReturnHandler;
			   }
			
			   public String getReturnValue() 
			   {
				   return returnValue;
			   }
			
			   public boolean isReturnValueIsJsException() 
			   {
				   return returnValueIsJsException;
			   }
		   }
			
		   private final WeakReference<WebViewBridgeFragment> webViewBridgeFragmentWeakReferance; 

		   WebViewJsMessagesHandler(WebViewBridgeFragment webViewBridgeFragment) {
			   webViewBridgeFragmentWeakReferance = new WeakReference<WebViewBridgeFragment>(webViewBridgeFragment);
		    }
		   @Override
		   public void handleMessage(Message msg)
		   {
		      if(msg.what == callNativeMessage){
		    	  final WebViewBridgeFragment webViewBridgeFragment = webViewBridgeFragmentWeakReferance.get();
		          NativeControllable nativeController = webViewBridgeFragment.nativeController;
		    	  if (nativeController != null) {
		    		  try
		    		  {
			        	  JSONObject obj = new JSONObject((String) msg.obj);
			        	  final String requestId = obj.getString("requestId");
			        	  String service = obj.getString("service");
			        	  String action = obj.getString("action");
	
			        	  JSONArray arr = obj.getJSONArray("params");
			        	  List<String> params = new ArrayList<String>();
			        	  for (int i = 0; i < arr.length(); i++)
			        	  {
			        	      params.add(arr.getString(i));
			        	  }
			      		  Log.d(this.getClass().getName(), "Received request from JS and sending to NativeController. RequestId=" + requestId + "; Service=" + service + "; Action=" + action);
			        	  nativeController.callNative(webViewBridgeFragment.getArguments().getInt(FRAGMENT_ID), service, action, params, new NativeCallReturnHandler() {
								@Override
								public void handleReturnFromNative(String returnValue, boolean isError) throws Exception {
									Log.d(this.getClass().getName(), "Returning value to JS for RequestId=" + requestId + "; returnValue=" + returnValue + "; isError=" + isError);
									String functionStatement = String.format(kReturnValueForJsRequest, requestId, returnValue, Boolean.toString(isError));
									webViewBridgeFragment.callJsInterface.callJs(functionStatement, null);
								}
			        	  });
		    		  }
		    		  catch (JSONException jsonEx)
		    		  {
		    			  Log.e(this.getClass().getName(), "Parsing '" + "' resulted in the following exception: " + jsonEx.toString());
		    		  }
		          }
		      }
		      else if(msg.what == jsCallReturnMessage)
		      {
		    	  WebViewJsMessagesHandler.JsCallReturnMessage jsCallReturnMessage = (WebViewJsMessagesHandler.JsCallReturnMessage) msg.obj;
		    	  jsCallReturnMessage.getWebViewJsCallReturnHandler().handleReturnFromJs(jsCallReturnMessage.getReturnValue(), jsCallReturnMessage.isReturnValueIsJsException());
		      }
		   }
	}

	public void loadWebview(WebView webView, LoadableUrl loadableUrl, boolean isReload) {
		LoadMode loadMode = loadableUrl.getLoadMode();
		if (loadMode == LoadMode.LoadCacheFirstWhenNoInet) {
			ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
			if (activeNetworkInfo == null) {
				webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
			} else {
				webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);//LOAD_DEFAULT);
			}
		} else if (loadMode == LoadMode.LoadNormal) {
			webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
		}
		
		 webView.loadUrl(loadableUrl.getUrl());
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.webView = new WebView(getActivity());
		 
		WebSettings webViewSettings = webView.getSettings();

		// JavaScript Features settings
		webViewSettings.setJavaScriptEnabled(true);
//		webViewSettings.setJavaScriptCanOpenWindowsAutomatically(false);

		// Various plug-ins settings
//		webViewSettings.setPluginsEnabled(true);
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO)
//		webViewSettings.setPluginState(WebSettings.PluginState.ON);


		//enable HTML5 App Cache settings
		webViewSettings.setDomStorageEnabled(true);
		webViewSettings.setDatabasePath("/data/data/" + webView.getContext().getPackageName() + "/databases/");
		// Set cache size to 8 mb by default. should be more than enough
		webViewSettings.setAppCacheMaxSize(1024*1024*8);
		webViewSettings.setAppCachePath(getActivity().getApplicationContext().getCacheDir().getAbsolutePath());
		webViewSettings.setAllowFileAccess(true);
		webViewSettings.setAppCacheEnabled(true);
		// Viewing preferences settings
		webViewSettings.setBuiltInZoomControls(true);
//		webViewSettings.setNeedInitialFocus(false);
//		webViewSettings.setLoadWithOverviewMode(true);
//		webViewSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
//		webViewSettings.setLoadsImagesAutomatically(true);

		// Geolocation settings
		webViewSettings.setGeolocationEnabled(true);
		//Next line not needed?
//		webViewSettings.setGeolocationDatabasePath("/data/data/net.compasspoint.spotter/databases");
		
		webViewJsMessagesHandler = new WebViewJsMessagesHandler(this);
		
		this.callJsInterface = new CallJSInterface(webViewJsMessagesHandler, webView);
		webView.addJavascriptInterface(callJsInterface, callJsInterface.getInterfaceName());

		

		final WebViewBridgeFragment fragment = this;
		webView.setWebChromeClient(new WebChromeClient() {
			 public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
				    callback.invoke(origin, true, false);
			 }
			 public void onProgressChanged(WebView view, int progress) {
				 // Activities and WebViews measure progress with different scales.
				 // The progress meter will automatically disappear when we reach 100%
				 getActivity().setProgress(progress * 1000);
				 //Log.d(this.getClass().getName(), "OnProgressChanged: " + Integer.valueOf(progress).toString());
			 }
			  
			@Override
			public boolean onJsAlert(WebView view, String url, String message,
					JsResult result) {
		            Log.d(this.getClass().getName(), message);
					Toast.makeText(fragment.getActivity(), message, Toast.LENGTH_LONG).show();
		            result.confirm();
		            return true;  
			}
			
		 });
	}

   @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_web_view_bridge, container, false);
		webView.setWebViewClient(new WebViewClient() {
			private boolean isLoadError = false;

			@Override
			public void onPageStarted(WebView webView, String url, Bitmap favicon) {
				Log.d(this.getClass().getName(), "onPageStarted: " + url);
				if (isNativeControllableWebViewControllableSet)
				{
					nativeController.unsetWebViewControllable(getArguments().getInt(FRAGMENT_ID));
					isNativeControllableWebViewControllableSet = false;
				}
			}
		   
		   @Override
			public void onPageFinished(WebView view, String url) {
				Log.d(this.getClass().getName(), "onPageEnded: " + url);

				if (!isLoadError) {
					Log.d(this.getClass().getName(), "onPageEnded and isError==false. setting webview to visible");
					if (!isNativeControllableWebViewControllableSet)
					{
						if (isResumed()) {
							nativeController.setWebViewControllable(getArguments().getInt(FRAGMENT_ID), WebViewBridgeFragment.this);
						}
						isNativeControllableWebViewControllableSet = true;
					}
				} else {
					Log.d(this.getClass().getName(), "onPageEnded and isError==true");
				}
			}
			
		   public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			   this.isLoadError = true; 
			   Log.d(this.getClass().getName(), "onReceivedError: description: " + description + "; failingUrl: " + failingUrl);
		   }
		   
		   @Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
			   Log.d(this.getClass().getName(), "shouldOverrideUrlLoading: " + url);
			    //return true means the host application handles the url, while return false means the current WebView handles the url.
			   for (String prefix : urlPrefixesAllowedToOpenInBrowser) {
				   if (url.startsWith(prefix)) {
					   Uri uri = Uri.parse(url);
					   Intent intent = new Intent(Intent.ACTION_VIEW, uri);
					   startActivity(intent);
					   break;
				   }
			   }
			   return true;
			}
		 });

        
        webView.setVisibility(View.VISIBLE);

        RelativeLayout layout = (RelativeLayout) view;
		webView.setLayoutParams(new LayoutParams(
			 LayoutParams.MATCH_PARENT,
			 LayoutParams.MATCH_PARENT));
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		layout.addView(webView);
		
		loadWebview(webView, getNativeController().getLoadableUrl(this.getArguments().getInt(FRAGMENT_ID)), false);

        return view;
    }
   
   @Override
   public void onDestroyView() {
	   super.onDestroyView();
   };
	
	@Override
    public void onStart() {
        super.onStart();
        // The fragment is about to become visible.
    }

	
	@Override
    public void onResume() {
        super.onResume();
        if (isNativeControllableWebViewControllableSet) {
			nativeController.setWebViewControllable(getArguments().getInt(FRAGMENT_ID), this);
        }
    }

	@Override
    public void onPause() {
		super.onPause();
		if (isNativeControllableWebViewControllableSet)
		{
			nativeController.unsetWebViewControllable(getArguments().getInt(FRAGMENT_ID));
		}
    }
    
	@Override
    public void onStop() {
        super.onStop();
        // The fragment is no longer visible (it is now "stopped")
    }
    
	@Override
    public void onDestroy() {
		this.webView.stopLoading();
        super.onDestroy();
 
        this.callJsInterface = null;
        this.webView.setWebViewClient(null);
        this.webView.setWebChromeClient(null);
        this.webView.destroy();
        this.webView = null;
    }

     @Override
    public void onConfigurationChanged(Configuration newConfig) {
    	super.onConfigurationChanged(newConfig);
    }
}

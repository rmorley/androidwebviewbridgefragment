/*
   Copyright 2014 Compass Point, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package net.compasspoint.webviewbridge;

import java.util.List;

import net.compasspoint.webviewbridge.WebViewBridgeFragment.LoadableUrl;

/**
 * Activities attaching to fragments must implement this interface.
 * @author R
 *
 */
public interface NativeControllable 
{
	/**
	 * Called by a fragment when hosted javaScript sends native message.
	 * 
	 * @param webViewBridgeFragmentId The fragmentId of the calling fragment
	 * @param service The service name specified in JavaScript native call.
	 * @param action The action specified in JavaScript native call.
	 * @param params Params specified in JavaScript native call.
	 * @param nativeCallReturnHandler The handler used to return a value to calling JavaScript.
	 */
    public void callNative(int webViewBridgeFragmentId, String service, String action, List<String> params, NativeCallReturnHandler nativeCallReturnHandler);
	
    /**
     * Called by fragment to determine what url to load.
     * @param webViewBridgeFragmentId
     * @return
     */
    public LoadableUrl getLoadableUrl(int webViewBridgeFragmentId);
	
    /**
     * Sets fragment's WebViewControllable when JavaScript is available to call.
     * @param webViewBridgeFragmentId
     * @param webViewControllable
     */
    public void setWebViewControllable(int webViewBridgeFragmentId, WebViewControllable webViewControllable);
	
    /**
     * Unsets fragment's WebViewControllable when JavaScript is not available to call.
     * @param webViewBridgeFragmentId
     */
    public void unsetWebViewControllable(int webViewBridgeFragmentId);
}
